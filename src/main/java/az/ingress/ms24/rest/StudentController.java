package az.ingress.ms24.rest;

import az.ingress.ms24.dto.StudentDto;
import az.ingress.ms24.dto.StudentRequestDto;
import az.ingress.ms24.exception.NotFoundException;
import az.ingress.ms24.services.StudentService;
import jakarta.annotation.PostConstruct;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    public Page<StudentDto> getStudent(Pageable page) {

        log.info("Received page {} size {}", page);
        return studentService.list(page);
        //     log.info("Received auth header {} test header {}", authorization, testHeader);

    }

    @GetMapping("/{id}")
    public StudentDto getStudent(@PathVariable Long id) {
//        checkIfExists(id);

        return studentService.get(id);
    }

    @PostMapping
    public StudentDto createStudent(@RequestBody @Valid StudentRequestDto requestDto) {
        return studentService.create(requestDto);
//        final int size = studentMap.keySet().size();
//        Long id = ((long) size) + 1;
//        StudentDto studentDto =
//                StudentDto
//                        .builder()
//                        .id(id)
//                        .firstName(requestDto.getFirstName())
//                        .lastName(requestDto.getLastName())
//                        .studentNumber(requestDto.getStudentNumber())
//                        .age(requestDto.getAge())
//                        .build();
//        studentMap.put(id, studentDto);
//        return studentDto;
    }

    @PutMapping("/{id}")
    public StudentDto updateStudent(
            @PathVariable Long id,
            @RequestBody StudentRequestDto requestDto) {
//        checkIfExists(id);
//        StudentDto studentDto =
//                StudentDto
//                        .builder()
//                        .id(id)
//                        .firstName(requestDto.getFirstName())
//                        .lastName(requestDto.getLastName())
//                        .studentNumber(requestDto.getStudentNumber())
//                        .age(requestDto.getAge())
//                        .build();
//        studentMap.put(id, studentDto);
        return studentService.update(id, requestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable Long id) {
      studentService.delete(id);
    }

}
