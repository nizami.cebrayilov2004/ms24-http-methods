package az.ingress.ms24.services;

import az.ingress.ms24.dto.StudentDto;
import az.ingress.ms24.dto.StudentRequestDto;
import org.springframework.data.domain.Page;

import java.awt.print.Pageable;

public interface StudentService {
    StudentDto create(StudentRequestDto requestDto);

    StudentDto update(Long id, StudentRequestDto requestDto);

    StudentDto get(Long id);

    void delete(Long id);

    Page<StudentDto> list(Pageable page);
}
