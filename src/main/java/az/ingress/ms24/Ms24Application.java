package az.ingress.ms24;

import az.ingress.ms24.domain.StudentEntity;
import az.ingress.ms24.repository.StudentRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.domain.Specification;

@RequiredArgsConstructor
@SpringBootApplication
public class Ms24Application implements CommandLineRunner {

    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(Ms24Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        studentRepository.save(nizami());
//         studentRepository.save(rustam());

//studentRepository.findAll(idGreaterThan(2L))
//        .stream()
//        .forEach(System.out::println);
        studentRepository.findAll(idGreaterThan(2L))
                .stream()
                .forEach(System.out::println);

    }

    public static Specification<StudentEntity> idGreaterThan(Long id) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThan(root.get(StudentEntity.Fields.id), id);


    }
//        studentRepository.findByJpql("Nizami", "Jabrayilov")
//                .stream()
//                .forEach(System.out::println);
//
//        studentRepository.findBySql()
//                .stream()
//                .forEach(System.out::println);

//       studentRepository.findAllByIdGreaterThanAndFirstNameLike(1L, "%izam%")
//                .stream()
//                .forEach(System.out::println);

    //   studentRepository.findAllByIdGreaterThanAndLastNameLike(1L, "Ja%")
    //         .stream()
    //       .forEach(System.out::println);


//      studentRepository.findAllByIdGreaterThan(0L)
//            .stream()
//            .forEach(System.out::println);

    //studentRepository.save(nizami());
    // studentRepository.save(rustam());
//     studentRepository.findAll()
//         .stream()
//            .forEach(System.out::print);


    private StudentEntity nizami() {
        return StudentEntity
                .builder()
                .age(20)
                .firstName("Nizami")
                .lastName("Jabrayilov")
                .studentNumber("333333")
                .build();
    }

    private StudentEntity rustam() {
        return StudentEntity
                .builder()
                .age(20)
                .firstName("Rustam")
                .lastName("Jafarov")
                .studentNumber("858585")
                .build();
    }
}
