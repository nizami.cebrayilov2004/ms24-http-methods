package az.ingress.ms24.repository;

import az.ingress.ms24.domain.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity, Long>, JpaSpecificationExecutor<StudentEntity> {

    List<StudentEntity> findAllByIdGreaterThan(Long id);

    List<StudentEntity> findAllByIdGreaterThanAndFirstNameLike(Long id, String firstName);

    List<StudentEntity> findAllByIdGreaterThanAndLastNameLike(Long id, String lastName);

    //JPQL
    @Query(value = "SELECT s FROM StudentEntity s WHERE s.firstName=?1 AND s.lastName=?2")
    List<StudentEntity> findByJpql(String firstName, String lastName);

    //SQL
    @Query(value = "SELECT s.first_name FROM students s", nativeQuery = true)
        List<String> findBySql();
}
